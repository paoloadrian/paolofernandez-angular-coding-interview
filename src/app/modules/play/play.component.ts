import { Component, OnDestroy, OnInit } from '@angular/core';
import { QuestionsService } from '../../core/services/questions.service';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { switchMap, tap } from 'rxjs/operators';
import { QuestionModel } from '../../core/state/question.model';

@Component({
  selector: 'app-play',
  templateUrl: './play.component.html',
  styleUrls: ['./play.component.scss']
})
export class PlayComponent implements OnInit, OnDestroy {
  correctAnswers!: number;
  answered!: number;
  subscription!: Subscription;
  questions$ = this.questionsService.questions$;
  gettingQuestions$ = this.questionsService.gettingQuestions$;
  getQuestionsSubscription: Subscription = this.route.queryParams
    .pipe(switchMap(params =>
      this.questionsService.getQuestions({
        type: params.type,
        amount: params.amount,
        difficulty: params.difficulty
      })
    )).subscribe();


  constructor(
    private readonly route: ActivatedRoute,
    private readonly router: Router,
    private readonly questionsService: QuestionsService,
  ) { }

  ngOnInit(): void {
    this.subscribeToQuestions();
  }

  ngOnDestroy(): void {
    if (this.subscription) {
      this.subscription.unsubscribe();
    }    
  }

  onAnswerClicked(questionId: QuestionModel['_id'], answerSelected: string): void {
    this.questionsService.selectAnswer(questionId, answerSelected);
  }

  subscribeToQuestions() {
    this.subscription = this.questions$.subscribe((questions) => {
      if (!questions || questions.length === 0) return;
      this.correctAnswers = 0;
      this.answered = 0;
      questions.forEach(question => {
        if (question.selectedId) {
          const answer = question.answers.find(answer => answer._id === question.selectedId)
          if (!answer) return;
          this.answered++;
          if (!answer.isCorrect) return
          this.correctAnswers++;
        }
      });
      if (this.answered === questions.length) {
        alert(`Score: ${this.correctAnswers}/${this.answered}`);
        this.router.navigate(['/home']);
      }
    });
  }

}
